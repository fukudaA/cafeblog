//ページ表示
package com.cafeblog.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Helloコントローラー
 * 
 * @author prolabo
 * @version 1.0.0
 * @since
 */


@Controller
public class pageController {
	
	@Autowired
    UserRepository repository;

	@GetMapping(value = "/index")
	public String index() {
		
		return "index";
	}
	@PostMapping(value = "/index")
	public String indexPost() {
		
		return "redirect:/index";
	}
	
	@GetMapping(value = "/index-login")
	public String indexLogin() {
		
		return "index-login";
	}
	
	@PostMapping(value = "/index-login")
	public String indexLoginPost() {
		
		return "redirect:/index-login";
	}
	
//	@RequestMapping(value = "/create")
//	public String create() {
//		
//		return "create";
//	}
	
	@RequestMapping(value = "/create-after")
	public String createAfter() {
		
		return "create-after";
	}
	
//	@RequestMapping(value = "/login")
//	public String login() {
//		
//		return "login";
//	}
	
	@RequestMapping(value = "/login-admin")
	public String loginAdmin() {
		
		return "login-admin";
	}
		
	@RequestMapping(value = "/post-tmpl")
	public String postTmpl() {
		
		return "post-tmpl";
	}
	
	@RequestMapping(value = "/post1")
	public String post1() {
		
		return "post1";
	}
	
	@RequestMapping(value = "/post2")
	public String post2() {
		
		return "post2";
	}
	
	@RequestMapping(value = "/post3")
	public String post3() {
		
		return "post3";
	}
	
	@GetMapping(value = "/data")
	public ModelAndView data(ModelAndView model, Pageable pageable) {
		Page <User> list = repository.findAll(pageable);
        model.addObject("dataList",list);
        model.setViewName("data");
		return model;
	}
	
	

}