package com.cafeblog.springboot;

import java.util.HashSet;
import java.util.Set;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

public class DataDialect extends AbstractProcessorDialect {
	
	private static final String DIALECT_NAME = "Data Dialect";
	
	public DataDialect() {
		super(DIALECT_NAME, "data", StandardDialect.PROCESSOR_PRECEDENCE);
	}
	protected DataDialect(String name, String prefix, 
			int processorPrecedence) {
		super(name, prefix, processorPrecedence);
	}

	@Override
	public Set<IProcessor> getProcessors(String dialectPrefix) {
		final Set<IProcessor> processors = new HashSet<IProcessor>();
		processors.add(new DataPageAttributeTagProcessor(dialectPrefix));
		return processors;
	}

}
