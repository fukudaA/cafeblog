package com.cafeblog.springboot;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;

public class SignupForm {
	
		//　登録時、文字の制限、エラー時のメッセージも記載
	
	@Min(3)
	@Pattern(regexp="[a-zA-Z0-9]")
    private String username;

    @Size(min=8)
    @Pattern(regexp="[a-zA-Z0-9]")
    private String password;

    @Email
    private String mailAddress;
    
    
    //　get、set
    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    
    
    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }
}

