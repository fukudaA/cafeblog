package com.cafeblog.springboot;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    public User findByUsername(String username);

    public User findByMailAddress(String mailAddress);	
    
    public Page<User> findAll(Pageable pageable);
}
