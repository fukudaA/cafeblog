package com.cafeblog.springboot;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
/**
 * 
 * サインアップフォーム
 * 
 * @author andou
 *
 */
public class SignupForm {

	// 登録時、文字の制限、エラー時のメッセージも記載

//	@Min(3)
//	@Max(250)
//	@Pattern(regexp="[a-zA-Z0-9]")
	@Pattern(regexp="^\\w{3,32}$")
	private String username;

	@Size(min = 8, max = 255)
	private String password;

	@Email
	@Size(min = 3, max = 255, message="3文字以上255文字以内で入力してください")
	private String mailAddress;

	// get、set

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
}
