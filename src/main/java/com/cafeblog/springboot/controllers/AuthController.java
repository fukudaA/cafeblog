package com.cafeblog.springboot.controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cafeblog.springboot.SignupForm;
import com.cafeblog.springboot.UserService;

/**
 * 
 * Authコントローラー
 * 
 * @author andou
 *
 */

@Controller
public class AuthController {

	@Autowired
	UserService userService;

	@GetMapping("/create")
	public String signup(Model model) {
		model.addAttribute("signupForm", new SignupForm());
		return "create";
	}

	@PostMapping("/create")
	public String signupPost(Model model, @Valid SignupForm signupForm, BindingResult bindingResult,
			HttpServletRequest request) {

		if (bindingResult.hasErrors()) {
			return "create";
		}
		try {
			userService.registerUser(signupForm.getUsername(), signupForm.getPassword(), signupForm.getMailAddress());

		} catch (DataIntegrityViolationException e) {
			model.addAttribute("signupError", true);

			return "create";
		}
		try {
			request.login(signupForm.getUsername(), signupForm.getPassword());
		} catch (ServletException e) {
			e.printStackTrace();
		}

		return "redirect:/create-after";
	}

	@RequestMapping("/")
	public String index() {
		return "redirect:/index";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@PostMapping("/login")
	public String loginPost() {
		return "redirect:/login-error";
	}

	@GetMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return "login";
	}

	@GetMapping("/messages")
	public String messages() {
		return "messages";
	}

	@GetMapping("/login-admin")
	public String loginadmin(HttpServletRequest request) {
		try {
			request.logout();
		} catch (ServletException e) {
			e.printStackTrace();
			return "login-admin";
		}
		return "login-admin";
	}

	@PostMapping("/login-admin")
	public String loginadminPost(Model model, @Valid SignupForm signupForm, BindingResult bindingResult,
			HttpServletRequest request) {
		if (bindingResult.hasErrors()) {
			return "redirect:/login-admin-error";
		}
		try {
			request.login(signupForm.getUsername(), signupForm.getPassword());
		} catch (ServletException e) {
			e.printStackTrace();
			return "redirect:/login-admin-error";
		}
		return "redirect:/data";
	}

	@GetMapping("/login-admin-error")
	public String loginadminError(Model model, HttpServletRequest request) {
		model.addAttribute("loginError", true);
		try {
			request.logout();
		} catch (ServletException e) {
			e.printStackTrace();
			return "login-admin";
		}
		return "login-admin";
	}

}
