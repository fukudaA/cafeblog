//ページ表示
package com.cafeblog.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cafeblog.springboot.User;
import com.cafeblog.springboot.UserRepository;

/**
 * pageコントローラー
 * 
 * @author fukuda
 * @version 1.0.0
 * @since
 */

@Controller
public class PageController {
	
	@Autowired
	UserRepository  repository;

	// トップ画面(ログイン前)
	@GetMapping(value = "/index")
	public String index() {

		return "index";
	}

	@PostMapping(value = "/index")
	public String indexPost() {

		return "redirect:/index";
	}

	// ログイン画面(ログイン後)
	@GetMapping(value = "/index-login")
	public String indexLogin() {

		return "index-login";
	}

	@PostMapping(value = "/index-login")
	public String indexLoginPost() {

		return "redirect:/index-login";
	}

	// 新規登録完了画面
	@RequestMapping(value = "/create-after")
	public String createAfter() {

		return "create-after";
	}

	// 管理者ログイン画面
	@RequestMapping(value = "/login-admin")
	public String loginAdmin() {

		return "login-admin";
	}

	// 投稿記事テンプレート
	@RequestMapping(value = "/post-tmpl")
	public String postTmpl() {

		return "tmpl-post";
	}

	// 各投稿記事
	@RequestMapping(value = "/post1")
	public String post1() {

		return "post1";
	}

	@RequestMapping(value = "/post2")
	public String post2() {

		return "post2";
	}

	@RequestMapping(value = "/post3")
	public String post3() {

		return "post3";
	}

	// ユーザー情報確認画面(管理者のみ)
//	@GetMapping(value = "/data")
//	public String data() {
//
//		return "data";
//	}

	@GetMapping(value = "/data")
	public ModelAndView data(ModelAndView model, Pageable pageable) {
		Page <User> list = repository.findAll(pageable);
        model.addObject("dataList",list);
        model.setViewName("data");
		return model;
	}

}