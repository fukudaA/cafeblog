package com.cafeblog.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class cafeblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(cafeblogApplication.class, args);
	}

}
