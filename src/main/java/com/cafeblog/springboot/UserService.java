package com.cafeblog.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Userサービス
 * 
 * @author andou
 *
 */

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository repository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		if (username == null || "".equals(username)) {
			throw new UsernameNotFoundException("Username is empty");
		} // 名前がnull、スペースの場合エラーを返す

		User user = repository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found: " + username);
		} // ユーザーが見つからない場合エラーを返す

		return user;
	}

	@Transactional // アドミ（初回）をデータベースに登録
	public void registerAdmin(String username, String password, String mailAddress) {
		User user = new User(username, passwordEncoder.encode(password), mailAddress);
		user.setAdmin(true);
		repository.save(user);
	}

	@Transactional // ユーザーをデータベースに登録
	public void registerUser(String username, String password, String mailAddress) {
		User user = new User(username, passwordEncoder.encode(password), mailAddress);
		repository.save(user);
	}

}