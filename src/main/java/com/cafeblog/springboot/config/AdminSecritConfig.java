package com.cafeblog.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.cafeblog.springboot.DataDialect;

import org.springframework.security.config.annotation.web.builders.WebSecurity;

/**
 * 
 * セキュリテコンフィング
 * 
 * @author andou
 *
 */

@EnableWebSecurity
@Order(2)
public class AdminSecritConfig extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/favicon.ico", "/css/**", "/js/**", "/images/**", "/fonts/**",
				"/shutdown" /* for Demo */);
	}

	@Override

	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
//				.antMatchers("/**").permitAll()
				.antMatchers("/login", "/login-error", "/login-admin", "/login-admin-error", "/logout").permitAll()
				.antMatchers("/**").hasRole("ADMIN")
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.loginProcessingUrl("/login-admin") // ログイン処理をするURL
				.loginPage("/login-admin")// ログインができるページ
				.failureUrl("/login-admin-error")// ログイン失敗
				.successForwardUrl("/data")// 認証成功時のURL
//				.passwordParameter("username")
//				.passwordParameter("password")
				.and()
			.logout()
				.logoutSuccessUrl("/index").permitAll()
//				.and()
//			.sessionManagement()
//				// セッションが無効な時の遷移先
//				.invalidSessionUrl("/messges")
				.and()
			.exceptionHandling()
				.accessDeniedPage("/login-admin-error");

	}
}
