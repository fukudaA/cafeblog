package com.cafeblog.springboot.config;

import org.springframework.beans.factory.annotation.Autowired;

//import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.cafeblog.springboot.DataDialect;
import com.cafeblog.springboot.UserService;

//import com.cafeblog.springboot.User;
//import com.cafeblog.springboot.UserService;

import org.springframework.security.config.annotation.web.builders.WebSecurity;

/**
 * 
 * セキュリテコンフィング
 * 
 * @author andou
 *
 */

@EnableWebSecurity
@Order(1)
public class WebSecritConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	@Override

	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/favicon.ico", "/css/**", "/js/**", "/images/**", "/fonts/**",
				"/shutdown" /* for Demo */);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
//				.antMatchers("/**").permitAll()
				.antMatchers("/", "/index", "/create**", "/login", "/favicon.ico",
						"/login-error", "/login-admin","/login-admin-error",
						"/css/**", "/js/**", "/img/**", "/fonts/**", "/scss/**",
						"/vendor/**",
						"/logout").permitAll()

				.antMatchers("/index-login", "/post**").hasRole("USER")
				.antMatchers("/**").hasRole("ADMIN")
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.loginProcessingUrl("/login") // ログイン処理をするURL
				.loginPage("/login")// ログインができるページ
				.failureUrl("/login-error")// ログイン失敗
				.successForwardUrl("/index-login")// 認証成功時のURL
				.and()
			.logout()
				.logoutSuccessUrl("/index").permitAll()
//				.and()
//			.sessionManagement()
//					// セッションが無効な時の遷移先
//				.invalidSessionUrl("/login-admin")
				.and()
			.exceptionHandling()
				.accessDeniedPage("/login-admin-error")// エラー時に遷移
			.and()
			.csrf().disable();
			
	}

//		　二回目以降は、コメント化
//		@Override
//		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//			auth
//				.userDetailsService(userService)
//				.passwordEncoder(passwordEncoder());
//
//			userService.registerAdmin("admin", "youmustchangethis", "admin@localhost");
//		}
//		　ここまでコメント化
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
    public ClassLoaderTemplateResolver templateResolver() {
    	ClassLoaderTemplateResolver templateResolver = 
    			new ClassLoaderTemplateResolver();
    	templateResolver.setPrefix("templates/");
    	templateResolver.setCacheable(false);
    	templateResolver.setSuffix(".html");        
    	templateResolver.setTemplateMode("HTML5");
    	templateResolver.setCharacterEncoding("UTF-8");
    	return templateResolver;
    }
	
	@Bean
	    public SpringTemplateEngine templateEngine(){
	    	SpringTemplateEngine templateEngine = new SpringTemplateEngine();
	    	templateEngine.setTemplateResolver(templateResolver());
	    	templateEngine.addDialect(new DataDialect());
	    	return templateEngine;
	}
}